package com.sb.blog.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.sb.blog.entity.Post;
import com.sb.blog.exception.ResourceNotFoundException;
import com.sb.blog.payload.PostDto;
import com.sb.blog.repository.PostRepository;
import com.sb.blog.service.PostService;

@Service
public class PostServiceImpl implements PostService {

//	Constructor based dependency injection
	private PostRepository postRepository;
	public PostServiceImpl(PostRepository postRepository) {
		this.postRepository = postRepository;
	}
	
//	Convert Entity to DTO
	private PostDto mapToDTO(Post post) {
		PostDto postDto = new PostDto();
		postDto.setId(post.getId());
		postDto.setTitle(post.getTitle());
		postDto.setDescription(post.getDescription());
		postDto.setContent(post.getContent());
		return postDto;
	}
	
//	Convert DTO to Entity
	private Post mapToEntity(PostDto postDto) {
		Post post = new Post();
		post.setTitle(postDto.getTitle());
		post.setDescription(postDto.getDescription());
		post.setContent(postDto.getContent());
		return post;
	}

	@Override
	public PostDto createPost(PostDto postDto) {
		Post post = mapToEntity(postDto);		
		PostDto postResponse = mapToDTO(postRepository.save(post));
		return postResponse;
	}

	@Override
	public List<PostDto> getAllPosts() {
		List<Post> posts = postRepository.findAll();
		return posts.stream().map(singlePost -> mapToDTO(singlePost)).collect(Collectors.toList());
	}

	@Override
	public PostDto getPostById(Long id) {
		Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
		return mapToDTO(post);
	}

	@Override
	public PostDto updatePost(PostDto postDto, Long id) {
		// get the post by id
		Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));

		// replacing the old_value with new_value
		post.setTitle(postDto.getTitle());
		post.setDescription(postDto.getDescription());
		post.setContent(postDto.getContent());
		
		// saving the data & converting to PostDto and return
		Post updatedPost = postRepository.save(post);
		return mapToDTO(updatedPost);
	}

}
